from machinetranslation import translator
from flask import Flask, render_template, request
import json
import machinetranslation

app = Flask("Web Translator")

@app.route("/englishToFrench")
def englishToFrench():
    textToTranslate = request.args.get('textToTranslate')
    result = machinetranslation.englishToFrench(textToTranslate)
    return "Translated text {} to French: {}".format(textToTranslate,result)

@app.route("/frenchToEnglish")
def frenchToEnglish():
    textToTranslate = request.args.get('textToTranslate')
    result = machinetranslation.frenchToEnglish(textToTranslate)
    return "Translated {} to English: {} ".format(textToTranslate,result)

@app.route("/")
def renderIndexPage():
    return render_template("index.html")

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
