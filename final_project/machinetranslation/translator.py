import os
from ibm_watson import LanguageTranslatorV3
from ibm_cloud_sdk_core.authenticators import IAMAuthenticator
from dotenv import load_dotenv

load_dotenv()

APIKEY = os.environ['apikey']
URL = os.environ['url']

authenticator = IAMAuthenticator(APIKEY)
LANGUAGE_TRANSLATOR = LanguageTranslatorV3(
    version='2018-05-01',
    authenticator=authenticator
)

LANGUAGE_TRANSLATOR.set_service_url(URL)

def englishToFrench(english_text):
    '''takes a english word and converts to french'''
    result = LANGUAGE_TRANSLATOR.translate(
        text=english_text,
        model_id='en-fr').get_result()
    french_text = result["translations"][0]["translation"]
    return french_text

def frenchToEnglish(french_text):
    '''Takes a french word and converts to english'''
    result = LANGUAGE_TRANSLATOR.translate(
        text=french_text,
        model_id='fr-en').get_result()
    english_text = result["translations"][0]["translation"]
    return english_text
