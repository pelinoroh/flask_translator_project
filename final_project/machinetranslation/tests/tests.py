import unittest

from translator import frenchToEnglish, englishToFrench

class TestFrenchNone(unittest.TestCase):
    def test1(self):
        with self.assertRaises(Exception, msg="input required"): 
            frenchToEnglish()/frenchToEnglish('')

class TestEnglishNone(unittest.TestCase):
    def test1(self):
        with self.assertRaises(Exception):
            englishToFrench()/englishToFrench('')

class TestFrenchToEnglish(unittest.TestCase):
    def test1(self):
        self.assertNotEqual(frenchToEnglish('Hello'),'Bonjour')
        self.assertEqual(frenchToEnglish('Bonjour'),'Hello')



class TestEnglishToFrench(unittest.TestCase):
    def test1(self):
        self.assertNotEqual(englishToFrench('Bonjour'),'Hello')
        self.assertEqual(englishToFrench('Hello'),'Bonjour')



if __name__ == '__main__':
    unittest.main()
